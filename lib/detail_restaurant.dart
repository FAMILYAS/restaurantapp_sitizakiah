import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailRestaurant extends StatefulWidget {
  const DetailRestaurant({Key? key}) : super(key: key);

  @override
  _DetailRestaurantState createState() => _DetailRestaurantState();
}

class _DetailRestaurantState extends State<DetailRestaurant> {
  List _items = [], _drink = [];

  @override
  void initState() {
    super.initState();
    _items = Get.arguments['menus']['foods'];
    _drink = Get.arguments['menus']['drinks'];

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: Colors.black, //change your color here
        ),
        elevation: 2,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.maxFinite,
          color: Colors.white,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                height: 150,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: NetworkImage(Get.arguments['pictureId']),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Text(
                Get.arguments['name'],
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 5),
              Row(
                children: [
                  Row(
                    children: [
                      const Icon(
                        Icons.location_on,
                        color: Colors.amber,
                        size: 13,
                      ),
                      const SizedBox(width: 3),
                      Text(Get.arguments['city']),
                    ],
                  ),
                  const SizedBox(width: 10),
                  Row(
                    children: [
                      const Icon(
                        Icons.star,
                        color: Colors.amber,
                        size: 13,
                      ),
                      const SizedBox(width: 3),
                      Text(
                        Get.arguments['rating'].toString(),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 15),
                width: double.maxFinite,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Description",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(Get.arguments['description']),
                  ],
                ),
              ),
              const SizedBox(height: 5),
              const Text(
                "Foods",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              for (int i = 0; i < _items.length; i++)
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0xFFe8e8e8),
                        blurRadius: 5.0,
                        offset: Offset(0, 2),
                      ),
                      BoxShadow(
                        color: Colors.white,
                        offset: Offset(-2, 0),
                      ),
                      BoxShadow(
                        color: Colors.white,
                        offset: Offset(2, 0),
                      ),
                    ],
                  ),
                  width: double.maxFinite,
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(bottom: 20),
                  child: Column(
                    children: [Text(_items[i]['name'])],
                  ),
                ),
              const SizedBox(height: 5),
              const Text(
                "Drinks",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              for (int i = 0; i < _drink.length; i++)
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0xFFe8e8e8),
                        blurRadius: 5.0,
                        offset: Offset(0, 2),
                      ),
                      BoxShadow(
                        color: Colors.white,
                        offset: Offset(-2, 0),
                      ),
                      BoxShadow(
                        color: Colors.white,
                        offset: Offset(2, 0),
                      ),
                    ],
                  ),
                  width: double.maxFinite,
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(bottom: 20),
                  child: Column(
                    children: [Text(_drink[i]['name'])],
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
